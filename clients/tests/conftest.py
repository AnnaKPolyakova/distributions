import pytest
from django.contrib.auth import get_user_model
from rest_framework.test import APIClient

from clients.tests.factories import ClientFactory, CodeFactory, TagFactory

User = get_user_model()

EMAIL = "Test@test.ru"
EMAIL_2 = "Test2@test.ru"
PASSWORD = "TestTestTest"
GENDER = "MALE"
FIRST_NAME = "Test"
LAST_NAME = "Test"


@pytest.fixture
def user():
    user = User.objects.create_user(
        username=EMAIL,
        email=EMAIL,
    )
    user.set_password(PASSWORD)
    user.save()
    return user


@pytest.fixture
def code():
    return CodeFactory()


@pytest.fixture
def tag():
    return TagFactory()


@pytest.fixture
def client(tag, code):
    return ClientFactory()


@pytest.fixture
def guest_client():
    client = APIClient()
    return client


@pytest.fixture
def authorized_client(user):
    client = APIClient()
    client.force_authenticate(user=user)
    return client
