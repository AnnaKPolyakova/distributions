import pytest
from django.urls import reverse

CLIENT_URL = reverse("clients-list")
CLIENT_DETAIL_URL = "clients-detail"

pytestmark = pytest.mark.django_db

PHONE = 71234567891
PHONE_2 = 71234567892
TIME_ZONE = "Europe/Moscow"


class TestClientAPI:

    def test_create_client_url(self, authorized_client, code, tag):
        url = CLIENT_URL
        data = {
            "code": code.id,
            "phone_number": PHONE,
            "time_zone": TIME_ZONE,
            "tag": tag.id,

        }
        response = authorized_client.post(url, data=data)
        assert response.status_code == 201, (
            f"Проверьте, что при POST запросе"
            f"{url} возвращается статус 201"
        )

    def test_update_client_url(self, authorized_client, code, tag, client):
        url = reverse(CLIENT_DETAIL_URL, args=[client.id])
        data = {
            "code": code.id,
            "phone_number": PHONE_2,
            "time_zone": TIME_ZONE,
            "tag": tag.id,

        }
        response = authorized_client.put(url, data=data)
        assert response.status_code == 200, (
            f"Проверьте, что при PUT запросе"
            f"{url} возвращается статус 200"
        )
        response = authorized_client.patch(url, data=data)
        assert response.status_code == 200, (
            f"Проверьте, что при PATCH запросе"
            f"{url} возвращается статус 200"
        )

    def test_delete_client_url(self, authorized_client, client):
        url = reverse(CLIENT_DETAIL_URL, args=[client.id])
        response = authorized_client.delete(url)
        assert response.status_code == 204, (
            f"Проверьте, что при DELETE запросе"
            f"{url} возвращается статус 204"
        )
