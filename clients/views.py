from drf_spectacular.utils import extend_schema, extend_schema_view
from rest_framework import mixins, viewsets
from rest_framework.permissions import IsAuthenticated

from clients.models import Client
from clients.schema.schema_extension import (
    CLIENT_RESPONSE_FOR_CREATE, CLIENT_RESPONSE_FOR_PARTIAL_UPDATE,
    CLIENT_RESPONSE_FOR_UPDATE)
from clients.serializers import ClientSerializer


@extend_schema_view(
    create=extend_schema(responses=CLIENT_RESPONSE_FOR_CREATE),
    update=extend_schema(responses=CLIENT_RESPONSE_FOR_UPDATE),
    partial_update=extend_schema(responses=CLIENT_RESPONSE_FOR_PARTIAL_UPDATE),
)
class ClientsViewSet(
    mixins.CreateModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    mixins.ListModelMixin,
    viewsets.GenericViewSet,
):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer
    permission_classes = (IsAuthenticated,)
