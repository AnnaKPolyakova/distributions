from django.urls import include, path
from rest_framework.routers import DefaultRouter

from clients.views import ClientsViewSet

router = DefaultRouter()
router.register(r"clients", ClientsViewSet, basename="clients")

clients_urls = [
    path("", include(router.urls)),
]

urlpatterns = [
    path("v1/", include(clients_urls)),
]
