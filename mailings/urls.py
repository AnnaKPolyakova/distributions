from django.urls import include, path
from rest_framework.routers import DefaultRouter

from mailings.views import MailingViewSet

router = DefaultRouter()
router.register(r"mailings", MailingViewSet, basename="mailings")

clients_urls = [
    path("", include(router.urls)),
]

urlpatterns = [
    path("v1/", include(clients_urls)),
]
