import os
import time

import requests
from django.utils import timezone
from dotenv import load_dotenv

from clients.models import Client
from mailings.celery import app
from mailings.loggers import mailing_tasks_logger
from mailings.models import Mailing, Message

load_dotenv()

TOKEN = os.getenv("TOKEN")
SENT_EMAIL_URL = "https://probe.fbrq.cloud/docs/v1/send/"
HEADERS = {"Authorization": f"Bearer {TOKEN}"}
ERROR_INFO = 'При отправке сообщения возникла следующая ошибка: "{error}"'
MESSAGE_INFO = (
    "Статус отправки сообщения с id {message_id} в рамках рассылки с "
    "номером id - {mailing_id} - {status}"
)
TASK_ON_INFO = "mailing_task запущен для рассылки с номером id - {id}"
CLIENTS_INFO_INFO = (
    "Найдено {count} клиентов, подходящих под данную рассылку - {id}"
)
TASK_FINISHED_INFO = (
    "mailing_task завершена для рассылки с номером id - {id}"
)
SENT_MESSAGES_INFO = (
    "Не удалось отправить все сообщения рассылки с номером id - {id}"
)

RESENT_INFO = (
    "Запуск повторной отправки неотправленных сообщений в рамках рассылки id "
    "- {id}"
)


def sent_messages(clients, mailing):
    clients_who_did_not_receive_messages = []
    for client in clients:
        if timezone.now() > mailing.end:
            break
        message, created = Message.objects.get_or_create(
            mailing=mailing,
            client=client
        )
        data = {
            "phone": client.phone_number,
            "text": mailing.text,
        }
        try:
            response = requests.get(
                SENT_EMAIL_URL, data=data, headers=HEADERS, timeout=5
            )
        except Exception as error:
            mailing_tasks_logger.error(ERROR_INFO.format(error=error))
        else:
            if response.status_code == 200:
                message.status = Message.Status.SENT
                message.save()
            else:
                clients_who_did_not_receive_messages.append(client)
            mailing_tasks_logger.info(
                MESSAGE_INFO.format(
                    message_id=message.id,
                    mailing_id=mailing.id,
                    status=message.status,
                )
            )
    if len(clients_who_did_not_receive_messages) != 0:
        mailing_tasks_logger.info(
            SENT_MESSAGES_INFO.format(
                id=mailing.id
            )
        )
    return clients_who_did_not_receive_messages


@app.task()
def mailing_task(mailing_id):
    mailing_tasks_logger.info(TASK_ON_INFO.format(id=mailing_id))
    mailing = Mailing.objects.get(id=mailing_id)
    clients = Client.objects.filter(code=mailing.code, tag=mailing.tag)
    mailing_tasks_logger.info(
        CLIENTS_INFO_INFO.format(
            count=clients.count(),
            id=mailing_id
        )
    )
    clients_who_did_not_receive_messages = sent_messages(clients, mailing)
    if len(clients_who_did_not_receive_messages) != 0:
        time.sleep(60 * 10)
        mailing_tasks_logger.info(
            RESENT_INFO.format(
                id=mailing.id
            )
        )
        sent_messages(
            clients_who_did_not_receive_messages, mailing
        )
    mailing_tasks_logger.info(TASK_FINISHED_INFO.format(id=mailing_id))
