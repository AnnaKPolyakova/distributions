import os

from celery import Celery

from config.settings import settings

os.environ.setdefault("DJANGO_SETTINGS_MODULE", settings.SETTINGS_FOR_CELERY)

app = Celery("mailings")

app.config_from_object("django.conf:settings", namespace="CELERY")

app.autodiscover_tasks()
