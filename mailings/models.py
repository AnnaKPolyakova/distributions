from django.core.exceptions import ValidationError
from django.db import models
from django.db.models import UniqueConstraint
from django.utils.translation import gettext_lazy as _

from clients.models import Client, Code, Tag

ERROR_MESSAGE_FOR_MAILING = (
    "Время окончания рассылки должно быть больше, чем время начала"
)


class Mailing(models.Model):
    start = models.DateTimeField(
        verbose_name="Время начала рассылки",
        help_text="Укажите время начала рассылки",
    )
    end = models.DateTimeField(
        verbose_name="Время окончания рассылки",
        help_text="Укажите время окончания рассылки",
    )
    text = models.TextField(
        verbose_name="Текст сообщения",
        help_text="Укажите текст сообщения",
    )
    tag = models.ForeignKey(
        Tag,
        on_delete=models.CASCADE,
        related_name="mailings",
        verbose_name="Тэг",
        help_text="Укажите тэг для фильтрации клиентов для осуществления "
                  "рассылки",
    )
    code = models.ForeignKey(
        Code,
        on_delete=models.CASCADE,
        related_name="mailings",
        verbose_name="Код мобильного оператора",
        help_text="Укажите код мобильного оператора для фильтрации клиентов "
        "для осуществления рассылки",
    )

    class Meta:
        verbose_name_plural = "Рассылки"
        verbose_name = "Рассылка"

    def __str__(self):
        return f"Начало: {self.start}, окончание: {self.end}"

    def clean(self):
        if not (self.start <= self.end):
            raise ValidationError(ERROR_MESSAGE_FOR_MAILING)


class Message(models.Model):
    class Status(models.TextChoices):
        NOT_SENT = "NOT SENT", _("Не отправлено")
        SENT = "SENT", _("Отправлено")

    time = models.DateTimeField(
        auto_now_add=True,
        verbose_name="Время отправки сообщения",
        help_text="Укажите время отправки сообщения",
    )
    status = models.CharField(
        choices=Status.choices,
        default=Status.NOT_SENT,
        max_length=40,
        verbose_name="Статус отправки сообщения",
        help_text="Выберите статус отправки сообщения",
    )
    mailing = models.ForeignKey(
        Mailing,
        on_delete=models.CASCADE,
        related_name="messages",
        verbose_name="Рассылка",
        help_text="Выберите рассылку",
    )
    client = models.ForeignKey(
        Client,
        on_delete=models.CASCADE,
        related_name="messages",
        verbose_name="Клиент",
        help_text="Выберите клиента",
    )

    class Meta:
        constraints = [
            UniqueConstraint(
                fields=["mailing", "client"],
                name="unique_mailing_client"
            ),
        ]
        verbose_name_plural = "Сообщения"
        verbose_name = "Сообщение"

    def __str__(self):
        return f"Клиент: {self.client}, рассылка: {self.mailing}"
