# Distributions

Технологии и требования:
```
Python 3.9+
Django
Django REST Framework
Poetry
```

Дополнительно сделано:

- тесты
- запуск проекта через docker-compose одной командой
- автодокументация по адресу /api/v1/schema/swagger-ui/
- повторная отправка сообщений в случае неудачи
- логирование отправки сообщений
- проверка запуска тестов через GitLab CI

### Настройки Docker

##### Установка

* [Подробное руководство по установке](https://docs.docker.com/install/linux/docker-ce/ubuntu/)

### Настройки Docker-compose

##### Установка

* [Подробное руководство по установке](https://docs.docker.com/compose/install/)

### Зависимости

Зависимости в проекте устанавливаются с помощью poetry.
* [документация с информацией по установке poetry](https://python-poetry.org/docs/cli/)

#### Перед запуском проекта создаем переменные окружения
Создаем в корне .env и добавляем в него следующие данные:

* `DJANGO_SECRET_KEY=`
* `DB_NAME=`
* `POSTGRES_USER=`
* `POSTGRES_PASSWORD=`
* `DB_PORT=`
* `TOKEN=`
* `SETTINGS_FOR_CELERY=`

SETTINGS_FOR_CELERY указываем следующие:
* "config.settings.settings" - для запуска всего проекта в контейнерах
* "config.settings.local" - для локальной разработки

## Запуск проекта полностью в контейнерах docker

* `docker-compose up --build`

Автоматический создается пользователь admin, пароль admin. БД наполняется 
тестовыми данными

#### Доступ

http://127.0.0.1/

#### Автодокументация

http://127.0.0.1/api/v1/schema/swagger-ui/

### Для Локальной разработки (bd и redis в контейнерах + приложения локально)

#### Создаем виртуальное окружение, устанавливаем зависимости
* `python -m venv venv`
* `source venv/bin/activate`
* `poetry install`

#### Команды для запуска bd в контейнере + приложения локально

* `docker-compose -f postgres-local.yml up --build` - создать и запустить контейнеры docker
* `python manage.py runserver --settings config.settings.local` - запускаем 
  проект
* `python manage.py makemigrations --settings config.settings.local` - создать миграции (миграции уже хранятся в репозитории, так что это опционально)
* `python manage.py migrate --settings config.settings.local` - применить миграции
* `python manage.py createsuperuser --settings config.settings.local` - создать суперпользователя
* `docker run -d -p 6379:6379 redis` - запускаем redis в контейнере
* `celery -A mailings worker -l info` - зупускаем воркер celery

##### Создаем виртуальное окружение, устанавливаем зависимости
* `python -m venv venv`
* `source venv/bin/activate`
* `poetry install`

#### Доступ

* http://127.0.0.1:8000

### Заполнение БД тестовыми данными

* `docker-compose exec web python manage.py  filldb` - в контейнере
* `python manage.py  filldb` - локально

### Запуск тестов 

* `docker-compose exec web pytest` - в контейнере
* `pytest` - локально
